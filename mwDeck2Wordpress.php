<?php
	/*
		mwDeck2Wordpress: Converts Magic Workstation-formatted decklists to Wordpress style tags
		Copyright (C) 2011 Joshua Justice

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <http://www.gnu.org/licenses/>.
	*/

	//WARNING
	//This software is NOT SECURE. If publicized, users can upload malicious files to your server.
	function get_card($line){
		$line = ltrim($line);
		list($number, $set, $cardname) = sscanf($line, "%d %s %[^$]s ");
		$cardname = preg_replace("/\(.*\)/", "", $cardname);
		if($number){
			print "$number $cardname\n";
		}
	}
	
	header("Content-type: text/plain");
	$deckfile = $_FILES['deck']['tmp_name'];
	$deck = file_get_contents($deckfile);
	$array = explode("\n", $deck); //One card per array entry
	print "[deck]\n"; // opening tag
	foreach($array as $value){
		get_card($value); // Print the cards in the middle
	}
	print "[/deck]\n"; // closing tag
?>